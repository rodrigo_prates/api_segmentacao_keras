from setuptools import find_packages, setup
setup(name="keras_segmentation_v2",
      version="0.2",
      description="Image Segmentation toolkit for keras",
      author="Prates",
      author_email='rodrigolp01@gmail.com',
      platforms=["any"],  # or more specific, e.g. "win32", "cygwin", "osx"
      license="MIT",
      url="https://bitbucket.org/petrecml/api_segmentacao_keras",
      packages=find_packages(),
      )